# Hra v teréne s Metaverse

Pri hraní jednotlivých zážitkov v Metaverse sa stretneš s niekoľkými opakujúcimi sa princípmi. Tie základné priblížia práve nasledujúce časti.

## Všadehrateľné zážitky

Zážitky, ktoré je možné spustiť kdekoľvek, sa v Metaverse aktivujú buď výberom z ponuky alebo oskenovaním príslušného QR kódu v aplikácii (pozri sekciu [Príprava pred hľadaním](priprava-zazitku)).

Ak zážitok obsahuje postavičku v zasadenú do prostredia, s ktorou môžeš interagovať, odporúčame nasmerovať telefón **na sever**[^1]. Navigovať k správnemu pohľadu ťa budú aj animované bodky.

## Skupiny zážitkov

Pokiaľ sa AR keška skladá z viacerých lokalít či zážitkov, môžu ich autori združiť do skupiny. Konkrétny zážitok otvoríš kliknutím na jeho kartu v zozname. 

Tie časti, ktoré si už s pohľadu hry dokončil, majú v pravom hornom rohu značku. Autor kešky sa môže rozhodnúť, či umožní opakované hranie alebo nie.

### Geolokalizácia

Jednotlivé zážitky môžu byť lokalizované na konkrétne miesto - a to buď pomocou GPS súradníc (čo je nám bližšie) alebo pomocu technológie iBeacon, ktorá nie je v našich končinách moc rozšírená.

Takéto zážitky majú v pravom hornom rohu symbol strelky kompasu. Otvoriť sa dajú len v prípade, že sa zariadenie nachádza v okruhu 40 metrov od súradníc určených autorom alebo je v dosahu iBeaconu. Bohužiaľ, aplikácia neinformuje hráčov o tom, že majú "v dosahu" nejaký zážitok, je preto potrebné naštudovať si pokyny autora skrýše.

### Mapa miest

Keď sa v skupine zážitkov nachádzajú také, ktoré majú určenú polohu pomocu súradníc, zobrazuje sa v pravom dolnom rohu obrazovky aj **okrúhle tlačidlo ďalekohľadu**, ktoré otvorí 3D mapu s pohľadom na polohu hráča a jeho blízke okolie. Príslušné zážitky sa zobrazujú na mape a keď prídeš do blízkosti niektorého z nich, môžeš ho otvoriť a splniť úlohu od tvorcu.

!!! bug "3D mapa zvykne padať"
    Jednou z chýb, ktoré sme počas príprav odhalili na viacerých zariadeniach, je práve pád Android aplikácie pri otvorení mapy s dostupnými zážitkami ku konkrétnej keške. Na problém sme už upozornili autorov a veríme, že sa čoskoro vyrieši.

## Interakcia so zážitkom

Metaverse je silno príbehová platforma, ktorá sa snaží hráčov vtiahnuť do deja aj prostredníctvom interakcie s postavami. Zvyčajne sa stretneš s týmito situáciami:

### Dialóg

Postava vie byť ukecaná a rozprávať pokyny. Ide o jednoduché zobrazenie, kde nad konkrétnou vizualizáciou vidíš textovú "bublinu" s obsahom. 

V dialógu sa posunieš **pomocou tlačidla dole** (zobrazuje buď jednoduchú šípku smerujúcu doprava prípadne text pripravený autorom) alebo automatickým prepínaním. V prípade automatického posunu (ak tak daný dialóg nastavil jeho tvorca) sa zobrazuje odpočet sekúnd v hornej časti obrazovky.

### Výber možností

Dialóg a smotné akcie sa môžu rôzne vetviť, vďaka čomu v rámci zážitku vznikajú možnosti tvorby kvízu či upravenia deja podľa tvojich doterajších krokov. Tak, ako pri jednoduchom dialógu pokračuješ voľbou jediného tlačidla ďalej, tu dostaneš na výber dve alebo viac možností. 

!!! hint "Nezabudni"
    Tvorca zážitku sa môže rozhodnúť, že na základe tvojej voľby sa vykonajú následné akcie, ktoré môžu byť pre príbeh nezvratné. Nespoliehaj sa a **nepoužívaj systémové tlačidlo Späť na svojom zariadení**. Veľmi pravdepodobne si nezachrániš krk, v niektorých situáciach si zablokuješ cestu ďalej.

### Zadanie odpovede

Hra ťa môže vyzvať na zadanie písanej odpovede. Odporúčame nasledovať pokyny autora, pretože ak autor nenastaví kontrolu vstupu inak, tak Metaverse **rozlišuje všetky medzery, veľkosť písmen, diakritiku a podobne**.

Vstup môže mať podobu klasického textu, číslic alebo hviezdičkami maskovaného "hesla".

### Inventár




[^1]: Metaverse umiestni postavu na sever vtedy, keď nevie inak rozoznať polohu zariadenia v priestore.