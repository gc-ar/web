# Často kladené otázky o Metaverse

Túto sekciu aktualizujeme na základe otázok, ktoré sa k nám od hráčov dostanú. 

!!! question "Aplikácia funguje, ale na mieste nevidím postavičku"
    Tento problém sa týka širokého počtu zariadení so systémom Android. Je potrebné kliknúť v scéne, ktorá nefunguje, na ikonu ⋮ _(tri bodky)_ vpravo hore na obrazovke, v zobrazenom spondom menu zvoliť ikonu **No AR** a kliknúť na tlačidlo _Close_.

            
!!! question "Pri skenovaní kódu sa zobrazuje neostrý obraz"
    Táto chyba sa vyskytuje ojedinele na niektorých mobilných telefónoch. Pokiaľ vidíš, že obraz z kamery nie je ostrý, stačí zatvoriť obrazovku na skenovanie kódu a znova ju otvoriť. Malo by to vyriešiť problém, kým príde opravená verzia aplikácie.
    
!!! question "Je potrebné pripojenie k internetu?"
    **Áno.** Aplikácia využíva pripojenie k internetu na priebežné vyhodnocovanie prostredia, ukladanie stavu hry, inventára či dialógov. Je preto nevyhnutné mať v mobile s hrou dostupné pripojenie k internetu. Sú lokality s voľnými Wi-Fi sieťami, vzhľadom na ich rôznorodú úroveň zabezpečenia však odporúčame využiť dôveryhodné pripojenie. Dostupnosť služieb mobilného internetu si môžeš preveriť u svojho operátora.

!!! question "Som na správnom mieste a aplikácia nechce zobraziť žiadny obsah."
    Metaverse má niekedy problémy so zobrazením obsahu, keď hráč drží mobil "na ležato", akoby fotil danú scénu. Odporúčame vyskúšať otvoriť zážitok v klasickom portrait otočení.    