# Príprava pred hrou

Zážitok prostredníctvom Metaverse sa dá spustiť dvoma spôsobmi - vyhľadaním vo verejnom zozname alebo oskenovaním špecifického QR kódu s hrou. Nižšie si rozoberieme oba postupy.

## Skenovanie QR kódu

Najrýchlejším spôsobom, ako otvoriť pripravené zážitky, je načítanie špeciálneho QR kódu. Autor kešky môže v listingu svojej skrýše uviesť vygenerovaný kód, ktorý po zosnímaní skenerom z hlavnej obrazovky aplikácie otvorí priamo daný AR zážitok, alebo zobrazí skupinu dostupných miest.

[![Hlavná obrazovka](assets/priprava-zazitku/01.jpg)](assets/priprava-zazitku/01.png) 
[![Otvorený zážitok](assets/priprava-zazitku/03.jpg)](assets/priprava-zazitku/03.png) 
[![Skupina zážitkov](assets/priprava-zazitku/06.jpg)](assets/priprava-zazitku/06.png)
  
Ak sa jedná priamo o zážitok, zobrazí sa profilová stránka s tlačidlom **Play** uprostred. Skupiny viacerých zážitkov obsahujú prehľad jednotlivých častí, ktoré sa spúšťajú samostatne.

## Výber z ponuky

Pokiaľ autor zverejní svoj zážitok všetkým používateľom, je možné ho vyhľadať prostredníctvom integrovaného katalógu. Na úvodnej obrazovke aplikácie si môžeme pozrieť obľúbené zážitky zoradené do rôznych kategórií.

[![Ponuka voľne dostupného obsahu](assets/priprava-zazitku/02.jpg)](assets/priprava-zazitku/02.png) 


## Vyhľadávanie

Ak však chceme vyhľadať konkrétnu kešku, je potrebné pozrieť sa do listingu na pokyny tvorcu skrýše. Môže tam uviesť meno skupiny zážitkov, ktoré keď začneme písať do vyhľadávania, aplikácia automaticky doplní.

Alternatívu k vyhľadávaniu podľa názvu zážitku je hľadanie podľa používateľského mena. Metaverse nám tiež ponúkne automatické doplňovanie. Profily hráčov sa líšia od ostatných zážitkov tým, že začínajú znakom **@**_menoHráča_.

 [![Hľadanie podľa názvu](assets/priprava-zazitku/05.jpg)](assets/priprava-zazitku/05.png) 
 [![Hľadanie podľa používateľa](assets/priprava-zazitku/04.jpg)](assets/priprava-zazitku/04.png) 
 
