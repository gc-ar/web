# Prvé spustenie Metaverse

## Úvodná obrazovka

Pri prvom spustení Metaverse sa načíta rovno taká úvodná obrazovka, akú budeš bežne využívať. Obsahuje však niekoľko prvkov, na ktoré sa v tomto návode zameriame, aby bol tvoj zážitok s Metaverse čo najlepší.

### Povolenie lokalizácie zariadenia

Zážitky v Metaverse môžu byť naviazané na lokality reprezentované konkrétnym súradnicovým bodom. Fialový blok textu na obrazovke informuje o potrebe povolenia snímania polohy zariadenia. Klikneme na tlačidlo **Grant**, po čom sa zobrazí otázka, či chceme povoliť prístup k polohe zariadenia. Klikneme na **Povoliť**.

[![Zariadenie nemá práva](assets/prve-spustenie/01.jpg)](assets/prve-spustenie/01.png)
[![Otázka systému](assets/prve-spustenie/02.jpg)](assets/prve-spustenie/02.png)

Po povolení fialový oznam zmizne. Budeme pokračovať prihlásením používateľa.

## Používateľ

!!! warning "Pozor!"
    V apliácii Metaverse sa dá hrať aj bez prihlásenia, ale niektoré zážitky si ukladajú priebeh hry do profilu hráča. Z toho dôvodu **odporúčame** prihlásenie sa do aplikácie.
    
Kliknutím na sivú ikonu používateľa _vľavo dole_ sa dostaneme na obrazovku s informáciami. Namiesto informácií o našom účte sa zobrazujú fiktívne údaje a pod nimi panel na prihlásenie. Môžeme si zvoliť prihlásenie cez Facebook alebo e-mail.

[![Neprihlásený používateľ](assets/prve-spustenie/03.jpg)](assets/prve-spustenie/03.png) 

### Registrácia e-mailom

Aplikácia priamo predpokladá, že sa chceš zaregistrovať, preto ti ako prvé zobrazí registračný formulár **Create Account**. Ak už máš účet v službe Metaverse, klikni na fialový odkaž **Already have an account? Log in** a  pokračuj v ďalšej sekcii s prihlásením.

[![Vytvorenie účtu](assets/prve-spustenie/04.jpg)](assets/prve-spustenie/04.png)

Ak si chceš vytvoriť nový Metaverse účet, vyplň e-mailovú adresu a vymysli si dobré heslo[^1]. Kliknutím na tlačidlo Sign Up sa overí registrácia a v prípade úspechu sa zobrazí možnosť zvoliť si svoju prezývku v aplikácii.

[![Vytvorenie účtu vyplnené údaje](assets/prve-spustenie/05.jpg)](assets/prve-spustenie/05.png) 
[![Vytvorenie účtu vyplnené údaje](assets/prve-spustenie/07.jpg)](assets/prve-spustenie/07.png)

Po potvrdení sa dostaneš opäť na obrazovku profilu, odkiaľ budeme pokračovať na [povolenie snímania vstupov](#povolenie-snimania).

[![Prihlásený používateľ](assets/prve-spustenie/08.jpg)](assets/prve-spustenie/08.png)

### Prihlásenie e-mailom

Pokiaľ si sa už na Metaverse registroval, stačí vyplniť svoje údaje na stránke **Log in** a tvoj účet bude hneď dostupný v mobilnom telefóne.

[![Prihlásenie vyplnené údaje](assets/prve-spustenie/06.jpg)](assets/prve-spustenie/06.png)

### Prihlásenie cez Facebook

Ak si namiesto e-mailu zvolil možnosť prihlásenia cez Facebook, aplikácia ťa automaticky naviguje do Facebook appky, alebo ak ju nemáš, na webstránku v prehliadači, kde potvrdíš práva na prístup k tvojim údajom.

Facebook ich následne poskytne Metaverse, vytvorí sa ti účet a budeš si môcť zvoliť prezývku v aplikácii.

## Povolenie snímania

Aby mohla aplikácia Metaverse poskytovať všetky potrebné služby, vyžaduje povolenie na snímanie obrazu a zvuku.

V prvom kroku otvoríme skener kódu z hlavnej obrazovky. Klikneme na tlačidlo **Scan now**, čím sa otvorí rozhranie na načítanie kódu, ktorý priamo otvára niektorý z pripravených "zážitkov". 

Pokiaľ aplikácia nemala povolenie na použitie fotoaparátu, vypýta si ho práve teraz. Klikneme na **Povoliť**. Následne môžeme oskenovať QR kód, cez ktorý sa načíta zvolený zážitok.  


[![Povolenie kamera](assets/prve-spustenie/09.jpg)](assets/prve-spustenie/09.png) 
[![Skenovanie kódu](assets/prve-spustenie/10.jpg)](assets/prve-spustenie/10.png)

!!! tip
    Ak máš problém pri skenovaní Metaverse kódov, pozri sa do časti [Riešenie problémov](faq)
    
Pri otvorení zvoleného zážitku kliknutím na ikonu **Play** uprostred obrazovky si aplikácia vypýta povolenie na posledné z práv, ktoré potrebujeme prideliť - prístup k mikrofónu. Toto povolenie býva v niektorých systémoch spojené s možnosťou zaznamenávať video prostredníctvom kamery (keďže ide o spojenie vizuálneho aj zvukového záznamu).

[![Profil zážitku](assets/prve-spustenie/11.jpg)](assets/prve-spustenie/11.png) 
[![Pvolenie mikrofónu](assets/prve-spustenie/12.jpg)](assets/prve-spustenie/12.png)


Pokiaľ po týchto krokoch všetko funguje ako má, môžeme sa poobzerať po AR keškách v okolí. Príprave na ich riešenie v teréne sa venuje [ďalšia sekcia](priprava-zazitku).    

!!! warning "Upozornenie na nestabilitu"
    Metaverse využíva pokročilé senzory zariadení, ktoré nebývajú plne podporované mnohými výrobcami. Autori kešiek aktívne oznamujú tvorcom problémy pri riešení, ale tiež prosia o trpezlivosť s novou technológiou.