# Inštalácia Metaverse

Postup ako nainštalovať prostredie Metaverse, v ktorom sa dajú hrať jednotlivé kešky. Oficiálne podporované sú zariadenia so systémom **iOS 9.3 a vyšším** (iPhone, iPad a iPod Touch) a tiež zariadenia so systémom **Android 4.4 a vyššie**. Inštalácia aplikácie je zadarmo.

Pri inštalácii z oficiálneho obchodu sa preverí aj ďalšia kompatibilita zariadenia (najmä pri telefónoch s Androidom). 

Kliknutím na tlačidlo nižšie podľa toho, aký máš mobilný telefón, nainštaluješ aplikáciu Metaverse:


[<img src="../../assets/app-store-badge.svg" height="64px"/>](https://itunes.apple.com/app/id1159155137) 
[<img src="../../assets/google-play-badge.svg" height="64px"/>](https://play.google.com/store/apps/details?id=com.gometa.metaverse&hl=sk)
