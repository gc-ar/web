# Čo je Metaverse?

Platforma Metaverse dostupná na stránke [gometa.io](https://gometa.io) je produkt tvorený tímom ľudí okolo známych technologických firiem a zahraničných médií. Zameranie mnohých zamestnancov Metaverse, Inc. na storytelling je viditeľný aj v samotných možnostiach.

Metaverse ponúka hráčom možnosť zadarmo pracovať s výtvormi v augmentovanej realite. Vyniká najmä vo vytváraní "dialógových" zážitkov. 

Okrem zobrazovania augmentovaného obsahu sa snažili tiež prepájať s rôznymi zarideniami alebo externými službami. Dnes sú však aj pod vpyvom využitia pre geocaching tieto funkcie potlačené a vývojári sa sústreďujú na zlepšovanie herného zážitku.

Geocacheri tu ocenia možnosť umiestňovať rôzne AR prvky (obrázky, videá, postavičky či 3D modely) do priestoru a následne s nimi zmysluplne "pracovať" (viesť dialógy, tvoriť si inventár, sprevádzať dejom a podobne).
