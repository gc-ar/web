# Vitajte

Tieto stránky sú určené pre geocacherov - hľadačov aj tvorcov skrýš - zaujímajúcich sa o využitie technológií augmentovanej (rozšírenej) reality pri geolokačných a edukačných hrách.

Ak hľadáte všeobecné informácie o geocachingu, môžete si pozrieť [oficiálne stránky Geocaching.com](https://www.geocaching.com/guide/). 

## Informácie pre hráčov

* Základný **[sprievodca aplikáciou Metaverse](metaverse/info)**
* Základný **[sprievodca aplikáciou HP Reveal](reveal/info)**
* Pozri si [zoznam publikovaných **slovenských AR kešiek**](slovenske-ar-kesky)
* [**Tipy & triky**](tipy-triky) na riešenie jednoduchých problémov

## Máš chuť vytvoriť vlastnú AR kešku?

Skúsil si AR kešku a máš nápad, ako využiť tieto nové technológie pri vlastnej tvorbe. My sme na tom podobne! Pripravili sme preto [sprievodcu pre autorov základnými pravidlami a nástrojmi](ako-vytvorit).
