description: Pozri si mapu a zoznam nových AR kešiek

# Zoznam slovenských AR kešiek

!!! tip "Mapa slovenských AR kešiek"
    Ak sa chceš pozrieť, kde všade sa nachádzajú slovenské AR kešky, [otvor si túto mapu](https://www.geocaching.com/map/default.aspx?asq=Ym09Qk01OTc2Ng%3d%3d). (<a href="https://www.geocaching.com/map/default.aspx?asq=Ym09Qk01OTc2Ng%3d%3d" title="Otvoriť mapu v novom okne" target="_blank">v novom okne</a>)


| GC kód | Názov | Lokalita | Autor | Technológia | Publikovaná |
| ------ | ----- | -------- | ----- | ----------- | ----------- |
| [GC7QXKV](https://coord.info/GC7QXKV) | AR_Sprievodca DSOK | Kanianka | CR(G)EO | Metaverse | 16.06.2018 |
| [GC3N7KW](https://coord.info/GC3N7KW) | AR_Pamätihodnosti Dúbravky | Bratislava | :ITAL | Metaverse | 16.06.2018 |
| [GC7R0W8](https://coord.info/GC7R0W8) | AR_Botanik | Bratislava | kikonan | Metaverse | 16.06.2018 |
| [GC7QZMK](https://coord.info/GC7QZMK) | AR_Pribeh psika Nera | Nitra | klipers | Metaverse | 16.06.2018 |
| [GC7R99F](https://coord.info/GC7R99F) | AR_Chceme trvalý mier! | Nitra | klipers | HP Reveal | 17.06.2018 |
| [GC7T7HQ](https://coord.info/GC7T7HQ) | AR_ Dračí jazdec | Púchov | 3L1T4 | Metaverse | 06.07.2018 |
| [GC7TBVT](https://coord.info/GC7TBVT) | AR_ Národný cintorín | Martin | Markol. | HP Reveal | 11.07.2018 |
| [GC7QT7Y](https://coord.info/GC7QT7Y) | AR_Deväť múz | Žilina  | JuRoot | Metaverse | 07.08.2018 |
| [GC7W0ZH](https://coord.info/GC7W0ZH) | AR_Yettiho posledná stopa-Zamestnaný | Domaniža  | Galky | HP Reveal | 15.08.2018 |
| [GC7W4A5](https://coord.info/GC7W4A5) | AR_Nostalgické peniažky | Nitra  | klipers | HP Reveal | 15.08.2018 |
| [GC7V2KC](https://coord.info/GC7V2KC) | AR_ Audiosprievodca | Považská Bystrica  | T:-*J | Metaverse | 17.08.2018 |
| [GC7VVXK](https://coord.info/GC7VVXK) | AR_ GPS: The Art of Geocaching | Bratislava | Rikitan | HP Reveal | 01.01.2019 |
| [GC81KDF](https://coord.info/GC81KDF) | AR_Netchvilski: Siedmy symbol | Nitra  | klipers | HP Reveal | 01.01.2019 |
| [GC81V4R](https://coord.info/GC81V4R) | AR_Nitriansky Hollywood | Nitra  | klipers | HP Reveal | 01.01.2019 |
| [GC84196](https://coord.info/GC84196) | AR_Mystique | Košice | kovbojkáči | HP Reveal | 10.03.2019 |


## Zoznam na stiahnutie

[Zoznam slovenských AR kešiek](https://www.geocaching.com/play/search?bm=BM59766) si môžeš pozrieť aj na Geocaching.com. Môžeš si tam vytvoriť Pocket Query[^1] a stiahnuť si všetky AR kešky publikované u nás.

Ak ťa zaujímajú podobné kešky inde vo svete, môžeš využiť [rozšírené vyhľadávanie na Geocaching.com](https://www.geocaching.com/play/search?ot=4&kw=AR_)[^1]. 

[^1]: Vyžaduje sa Premium účet na stránkach Geocaching.com

## Archivované kešky

V tomto prehľade sa nachádzajú AR kešky, ktoré boli archivované autormi, nie je možné ich riešiť alebo nájsť v teréne.


| GC kód | Názov | Lokalita | Autor | Technológia | Publikovaná | Archivovaná |
| ------ | ----- | -------- | ----- | ----------- | ----------- | ----------- |
| [GC7WF50](https://coord.info/GC7WF50) | AR_Srdce | Žilina  | 9264 | HP Reveal | 24.08.2018 | 17.04.2019 |
| [GC7X855](https://coord.info/GC7X855) | AR_ My, počítačom opálení | Lúky (PU)  | T:-*J | HP Reveal | 05.09.2018 | 23.06.2019 |
| [GC7R4KK](https://coord.info/GC7R4KK) | AR_Back to the History: Žilina | Žilina | JuRoot | HP Reveal | 16.06.2018 | 30.06.2019 |
| [GC7QWCB](https://coord.info/GC7QWCB) | AR_♻️ | Žilina | JuRoot | HP Reveal | 17.06.2018 | 30.06.2019 |
| [GC81R8B](https://coord.info/GC81R8B) | AR_Ostrovy | Bytča  | JuRoot | HP Reveal | 01.01.2019 | 30.06.2019 |
| [GC81R8G](https://coord.info/GC81R8G) | AR_for_Jan | Štiavnik (BY)  | JuRoot | HP Reveal | 01.01.2019 | 30.06.2019 |
| [GC7R8MG](https://coord.info/GC7R8MG) | AR_mARa | Liptovský Trnovec | misoba | HP Reveal | 30.06.2018 | 01.07.2019 |
| [GC7RH6B](https://coord.info/GC7RH6B) | AR_Puzzle | Bratislava | \_miko\_ | HP Reveal | 03.07.2018 | 15.08.2019 |
| [GC6XXZ0](https://coord.info/GC6XXZ0) | AR_Back to the history: Ostredky | Bratislava | Rikitan | HP Reveal | 17.06.2018 | 18.09.2019 |
| [GC7DMF5](https://coord.info/GC7DMF5) | AR_Snake | Banská Bystrica | Mikme | HP Reveal | 12.01.2019 | 19.09.2019 |
| [GC8451Y](https://coord.info/GC8451Y) | AR_jarchiv - NTCH v ZM 🗺️ | Zlaté Morvce  | jarchiv | HP Reveal | 05.03.2019 | 20.09.2019 |
| [GC3E2FD](https://coord.info/GC3E2FD) | AR_Vyhliadková | Bratislava | :ITAL | HP Reveal | 20.06.2018 | 26.09.2019 |

