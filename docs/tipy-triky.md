# Riešenie problémov s AR keškami

Ak si narazil na problém s hraním AR kešiek, odporúčame ti pozrieť si najčastejšie tipy a triky na riešenie často sa vyskytujúcich prípadov. Rady sú rozdelené podľa aplikácie použitej na vytvorenie AR kešky. 

Momentálne tu nájdeš tipy pre:  
- [Metaverse](metaverse/faq.md)  
- [HP Reveal](reveal/faq.md)

