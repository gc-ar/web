description: Základné informácie o rozšírenej realite v geocachingu

# Čo je to AR?

_AR_ je skratka anglického označenia technológií "_augmented reality_", po našom **rozšírenej reality**. Často sa stretneme aj s názvom "**augmentovaná realita**" vychádzajúceho z medzinárodne používanej skratky.

Rozšírená realita využíva zariadenia na rozšírenie pohľadu na objekty v reálnom čase a danom kontexte, v ktorom sa nachádzajú. Využitie nachádza v marketingu, priemysle ale aj zábave, čo dáva základ pre *zaujímavé kešky*.

![Ukážka AR technológie v mobilnom telefóne](assets/ar_demo.jpg) 

Geocaching sa s podobnou technológiou nestretáva úplne prvý krát, autori aj hľadači skrýš však tentoraz dostali príležitosť vytvoriť *kešky v rozšírenej realite* s prižmúrením oka nad niektorými tradičnými pravidlami. Vďaka tomu v tomto období vznikajú skrýše, pri ktorých spoznávame možnosti aj limity pri využití v **outdoor aktivitách**. Bez ohľadu na použitú platformu je totiž zachovaný základný oporný bod geocachingu - hľadanie fyzickej schránky.

!!! tip "Slovenské AR kešky"
    Ak sa chceš pozrieť, ako vyzerajú a kde všade sa nachádzajú slovenské AR kešky, otvor si náš [zoznam a mapu slovenských AR kešiek](slovenske-ar-kesky.md).

_Zdroj ilustračnej foto: Flickr/Virtueel Platform (CC BY-SA 2.0)_