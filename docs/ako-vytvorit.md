# Ako vytvoriť vlastnú AR kešku?

!!! warning "Upozornenie"
    AR kešky sú momentálne publikované v rámci výnimky od Geocaching HQ ako experiment. Z toho dôvodu majú určené špecifické pravidlá, ktoré je potrebné poznať a vyžadujú viac komunikácie pri zakladaní s reviewerom.

## Základné zdroje

Predtým, ako sa pustíš do prípravy vlastnej kešky využívajúcej augmentovanú realitu, dobre si preštuduj nasledovné dokumenty:

- **[Špecifické pravidlá pre AR kešky](https://www.geocaching.com/help/index.php?pg=kb.chapter&id=127&pgid=921)** (dôležité!)  
- [Všeobecné pravidlá pre zakladanie kešiek](https://www.geocaching.com/play/guidelines)  

## Skupina pre autorov na Facebooku

Slovenskí tvorcovia AR kešiek diskutujú v skupine na Facebooku. Ak ťa zaujíma tvorba vlastnej kešky využívajúcej augmentovanú realitu, môžeš smelo [požiadať o pridanie](https://www.facebook.com/groups/438753153252013/about/).

## Výber vhodnej platformy

Výber platformy pre tvoju vlastnú AR kešku je úzko spojený s tým, aký spôsob zážitku chceš sprostredkovať hráčom a aké mechaniky k tomu potrebuješ. Zatiaľ máme len experimentálne skúsenosti s platformami Metaverse a HP Reveal. 

Geocaching HQ okrem týchto dvoch služieb umožňuje tiež tvorbu pomocou aplikácie Tale Blazer a ROAR. Ak máš chuť využiť ich pri tvorbe, budeme radi, keď sa podelíš o praktické skúsenosti v skupine alebo na týchto stránkach.

### Metaverse

Metaverse je platforma vyznačujúca sa svojim storytellingovým smerovaním. Hlavné možnosti predstavujú dialógy a akcie reprezentované charakterom v podobe obrázku či 3D modelu. Samotná knižnica pre tvorcov obsahuje veľké množstvo modelov od nezávislých autorov alebo z platformy [Google Poly](https://poly.google.com/).

Jednotlivé časti dialógu a vykonávanie logiky sa dá navrhnúť vo vizuálnom prostredí, ak si niekto trúfa na programovanie v JavaScripte, môže si vytvoriť vlastné logické blogy, ktoré sa dajú začleniť k existujúcej funkčnosti.

Všetky tieto možnosti sú dostupné v [publikovaných článkoch tvorcov](https://medium.com/metaverseapp) alebo v [komunitnom fóre](https://community.gometa.io/). Majú tiež k dispozícii [množstvo praktických ukážok](https://community.gometa.io/c/tutorials), kde vysvetľujú prácu s jednotlivými komponentami.

### HP Reveal

!!! warning "Ukončenie podpory"
    Tvorcovia HP Reveal oznámili koniec podpory štandardného prostredia na vytváranie AR zážitkov. Nové aury už nie je možné vytvárať, existujúce budú zmazané na konci júna 2019.


Platforma, ktorá už na prvý pohľad vyzerá serióznejšie, umocnená značkou silného technologického hráča. Medzi hlavné prednosti patrí **rozoznávanie známeho prostredia**, na ktoré je možné naviazať vrstvy. 

Samotné vrstvy môžu poskytovať zdroj ďalších podnetov pre aplikáciu, zobrazovať iné prvky alebo navigovať hráča na iné informačné zdroje. Vrstvy môžu obsahovať obrázky (aj transparentné), videá, animácie, 3D modely alebo samostatné audio.

Bližšie informácie sa nachádzajú v [oficiálnej dokumentácii HP Reveal](https://aurasma.zendesk.com/hc/en-us).   


!!! info
    Pre autorov kešiek zatiaľ nemáme kompletne spracovanú dokumentáciu. V priebehu najbližších týždňov budeme (spolu s rastúcimi skúsenosťami) budovať aj túto časť stránok. **Chceš nám pomôcť rozšíriť dokumentáciu? [Pozri sa ako na to.](chcem-pomoct)**

## Často kladené otázky

!!! question "Sú povolené aj indoor hry a stage?"
    Dobrá správa - **áno, sú** - ak sú ich výsledkom súradnice finálu, ktorý je vonku. Takže prípadné múzeá, knižnice, galérie, informačky, bane, podzemie, opustené budovy = dá sa.


## Banner do listingu

Ak chceš na tieto stránky s radami pre hráčov a tvorcov odkazovať v listingu, pripravili sme takýto banner, ktorého kód môžeš priamo použiť:

    <div><span>Ďalšie informácie v slovenčine o AR keškách nájdeš <a href="https://gc-ar.gitlab.io/web/" title="Geocaching AR" target="_blank">tu</a>.
    Postupne tam pribúdajú návody pre hranie a vytváranie AR hier v rôznych povolených aplikáciách.</span></div>

Ukážka banneru:

TODO