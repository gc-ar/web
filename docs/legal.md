# Právne informácie

"AR Geocaching" sú statické stránky informujúce širokú verejnosť o možnostiach využívania augmentovanej reality ako súčasti hry Geocaching. Stránky sú budované z verejného repozitára a poskytované v stave tak "ako sú". Prevádzkovateľ ani prispievatelia nepreberajú žiadne záruky za úplnosť, presnosť a pravdivosť uvedených údajov.

Hráči vyhlasujú, že všetky informácie tu uvedené využívajú pre vlastnú potrebu, dobrovoľne a bez nároku na akúkoľvek náhradu škôd, ku ktorým mohlo dôjsť v dôsledku využitia zverejených informácií. Aktivity hry geocaching sú ošetrené osobitnými pravidlami, ktoré sa zaviazal dodržiavať každý hráč využívajúci služby Geocaching.com.

## Ochrana osobných údajov

Stránky ako také nezbierajú **žiadne osobné informácie** (nepotrebujeme ich a nechceme) a k svojej činnosti nevyžadujú použitie súborov cookies. Stránka nevyužíva nástroje na monitorovanie návštevnosti a pohybu používateľa.

Výnimku tvoria podstránky typu "Poradňa", na ktorých je využitá služba **Disqus**. Všetky údaje, ktoré sa nachádzajú v komentárovej časti sú ukladané a spracovávané v súlade s Podmienkami ochrany osobných údajov spoločnosti Disqus, Inc. Podmienky ochrany osobných údajov a možnosti prispôsobenia práce s nimi sú dostupné na konkrétnych stránkach priamo pod formulárom na pridávanie príspevku. 

Využívanie týchto služieb nie je podmienkou k prezeraniu obsahu tejto stránky. Zamietnutie povolenia na spracovanie údajov v diskusných častiach môže mať za následok nevyhnutné obmedzenie prispievania do diskusného fóra (oprávnený záujem).

## Pravidlá diskusie

Správca v súlade s platnou legislatívou monitoruje a moderuje diskusiu. Vyhradzuje si plné právo odstrániť bez udania dôvodu ktorýkoľvek príspevok, najmä taký, ktorý zjavne porušuje pravidlá slušnej diskusie a/alebo platných zákonov.

Upozornenie na nevhodný obsah je možné vykonať prostredníctvom dostupných nástrojov v diskusnej časti stránky.

## Prehlásenia o použitých značkách

Na stránke sa nachádzajú zmienky o službách a značkách, ktoré sú chránené príslušným právom a medzinárodnými dohodami. Všetky prvky sú použité výlučne na nekomerčné účely v súlade so stanovenými podmienkami.

Logo Geocaching je registrovanou obchodnou známkou spoločnosti Groundspeak, Inc. Použité s povolením.

Google Play a Google Play Logo sú obchodnými známkami spoločnosti Google LLC.

Apple, Apple logo, iPhone, a iPad sú obchodnými známkami spoločnosti Apple Inc., registrovanými v Spojených štátoch Amerických a v iných krajinách a regiónoch. App Store je servisná značka spoločnosti Apple Inc.


## Prevádzkovateľ

Stránky využívajú verejnú službu na prevádzkovanie statickej dokumentácie GitLab Pages. Informácie o tejto službe sú dostupné [na tejto stránke](https://about.gitlab.com/features/pages/).

