# Inštalácia HP Reveal

Postup ako nainštalovať prehliadač HP Reveal, v ktorom sa dajú hrať jednotlivé kešky, sa v ničom nelíši od získavania iných aplikácií pre smartfóny. Oficiálne podporované sú zariadenia so systémom **iOS 8.0 a vyšším** (iPhone, iPad a iPod Touch) a tiež zariadenia so systémom **Android 4.1 a vyššie**. Inštalácia aplikácie je zadarmo.

Pri inštalácii z oficiálneho obchodu sa preverí aj ďalšia kompatibilita zariadenia. 

Kliknutím na tlačidlo nižšie podľa toho, aký máš mobilný telefón, nainštaluješ aplikáciu HP Reveal:

[<img src="../../assets/app-store-badge.svg" height="64px"/>](https://itunes.apple.com/us/app/hp-reveal/id432526396) 
[<img src="../../assets/google-play-badge.svg" height="64px"/>](https://play.google.com/store/apps/details?id=com.aurasma.aurasma&hl=sk)


