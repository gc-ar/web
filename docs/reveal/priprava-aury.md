# Príprava pred hrou

!!! tip "Stačí zvoliť jeden postup"
    Nasledujúce pokyny predstavujú **alternatívy** ako pripraviť svoje zariadenie na hľadanie konkrétnej kešky. Odporúčame dôkladne si prečítať konkrétny listing, kde ti autor poskytne bližšie informácie o tom, ktorá z možností sa vzťahuje na danú skrýšu.


## Mobilná aplikácia

Jednotlivé miesta, na ktorých sa zobrazujú aury (vytvorené AR zážitky), je potrebné najprv načítať do aplikácie v mobilnom telefóne. Toto sa dá docieliť kliknutím na tlačidlo **Follow** na verejnom profile autora či kešky samotnej.

### Mám názov kešky/autora

Verejný profil môžeš vyhľadať prostredníctvom ikony lupy na hlavnej obrazovke v aplikácii. Ak sa v listingu nachádzajú pokyny s uvedením mena profilu, môžeš začať písať tento názov do vyhľadávacieho poľa. Aplikácia sama ponúkne zodpovedajúce výsledky.

Na hlavnej obrazovke klikni do poľa **Discover Auras**. Vlož tam názov podľa pokynov od autora kešky. V príklade nižšie si vyhľadáme profil vytvorený na základe GC kódu danej kešky (pozor, nie je to podmienka!)

[![Vyhľadávacie pole](assets/priprava-aury/01.jpg)](assets/priprava-aury/01.png)  

Aplikácia zobrazí profil aj jednotlivé aury. **Pre správne fungovanie je potrebné zvoliť z výsledkov celý profil!** 

Ten spoznáme podľa názvu _xxx_**'s Public auras**. Navyše má vždy kruhovú ikonu, na rozdiel od ostatných výsledkov vyhľadávania. Ostatné aury spojené s keškou majú tiež iný názov.

[![Vyhľadané verejné aury](assets/priprava-aury/02.jpg)](assets/priprava-aury/02.png)

### Sledovanie profilu

Po zobrazení detailu správneho profilu (kešky) vidíš všetky zverejnené Aury, ktoré vytvoril autor. Aby si ich mohol použiť, musíš kliknúť na sivé tlačidlo **Follow**. V momente, ako zmení svoj nápis na _Following_ a farbu na modrú, môžeš začať s riešením úloh podľa listingu kešky.

[![Detail profilu pred sledovaním](assets/priprava-aury/03.jpg)](assets/priprava-aury/03.png)
[![Detail sledovaného profilu](assets/priprava-aury/04.jpg)](assets/priprava-aury/04.png)  

## Internetový prehliadač

Internetový prehliadač môžeš využiť na načítanie aury zdieľanej cez špeciálny odkaz v listingu či iným spôsobom. Tento postup sa hodí tiež na načítanie privátnych (neverejných) materiálov.

Odkaz na prístup ku konkrétnemu profilu či keške vyzerá nasledovne: https://**auras.ma/s/**_xxx_ 
  
_xxx_ je jedinečný prístupový kľúč pridelený autorovi.

### Prehliadač na mobile

Ak máš k dispozícii tento odkaz, odporúčame ho otvoriť v mobilnom telefóne [s pripravenou aplikáciou](prve-spustenie). Zobrazí sa úvodná obrazovka daného profilu. Keď sa presunieš nižšie, pod obrázkom nájdeš biele tlačidlo **Follow**.

[![Otvorený profil](assets/priprava-aury/05.jpg)](assets/priprava-aury/05.png)
[![Tlačidlo Follow](assets/priprava-aury/06.jpg)](assets/priprava-aury/06.png)

Kliknutím na toto tlačidlo sa zobrazí niekoľko možností. Rozoberieme si ich nižšie.

[![Výber](assets/priprava-aury/07.jpg)](assets/priprava-aury/07.png)

#### Sledovanie v aplikácii

Najvhodnejšie v tejto situácii je kliknúť na modré tlačidlo **Open in app to follow**. Pokiaľ máš všetko správne nastavené, spustí sa aplikácia HP Reveal a zobrazí sa prehľad dostupnej tvorby. Novopridaná keška sa bude zobrazovať v hornej časti. V tomto momente sa môžeš vrhnúť na hru podľa pokynov v listingu.

[![Pridané aury](assets/priprava-aury/08.jpg)](assets/priprava-aury/08.png)

#### Sledovanie cez prehliadač

Ak nemáš na danom zariadení nainštalovanú aplikáciu, ale chceš svoj účet spojiť s touto keškou, môžeš využiť zadanie svojich používateľských údajov.

Namiesto kliknutia na modré tlačidlo **vyplň prihlasovacie meno a heslo** a klikni na tlačidlo **Sign in**. Pokiaľ údaje súhlasia, zobrazí sa ti rovnaká stránka, ako na začiatku, ale tlačidlo Follow sa zmení na **zelené s nápisom Following**.

[![Vyplnené údaje](assets/priprava-aury/09.jpg)](assets/priprava-aury/09.png)
[![Všetko v poriadku](assets/priprava-aury/10.jpg)](assets/priprava-aury/10.png)


### Prehliadač na počítači

Spojiť kešku so svojim účtom môžeš aj cez prehliadač v počítači. Po otvorení prístupovej adresy (viď [úvod tejto sekcie](#internetovy-prehliadac)) na počítači môžeš jednoducho sledovať kešku.

Po zobrazení profilu môžeš kliknúť na **biele tlačidlo Follow**. Pokiaľ nie si prihlásený, stránka ťa vyzve na zadanie svojho mena a hesla. Po kliknutí na **Sign in** overí tvoje údaje a ak sedia, zmení sa biele tlačidlo na zelené s nápisom **Following**.

Pri najbližšom spustení mobilnej aplikácie sa načíta nová zmena a môžeš sa smelo vydať za riešením AR kešky.

[![Otvorená ponuka na počítači](assets/priprava-aury/11.jpg)](assets/priprava-aury/11.png)
[![Zadanie identifikačných údajov](assets/priprava-aury/12.jpg)](assets/priprava-aury/12.png)
[![Všetko v poriadku](assets/priprava-aury/13.jpg)](assets/priprava-aury/13.png)

!!! warning "Pozor"
    V aplikácii aj webovom prehliadači je potrebné použiť rovnaké meno používateľa. V opačnom prípade nedôjde k správnemu prepojeniu tvojho účtu s pripravenou keškou a aury nebudú fungovať!