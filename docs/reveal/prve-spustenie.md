# Prvé spustenie HP Reveal na mobile

## Android
### Pridelenie práv

Nové systémy (od verzie Android 6 vyššie) vyžadujú osobitné povolenia práv na prácu s niektorými funkciami systému. HP Reveal vyžiada osobitne **tri práva**: na prístup **k polohe zariadenia** (aby boli použiteľné aury s nastavenou lokalitou), **k súborom v zariadení** (aby bola dostupná funkcia vytvárania obrázkov alebo videí z riešenia kešiek) a **na prístup ku kamere** (aby aplikácia mohla "pozerať" cez objektív mobilu a rozoznávať objekty).

Ak vidíš obrazovky podobné tým nižšie, klikni na každej z nich na tlačidlo **Povoliť**.

[![Práva k polohe zariadenia](assets/prve-spustenie/01.jpg)](assets/prve-spustenie/01.png) [![Práva k súborom v zariadení](assets/prve-spustenie/02.jpg)](assets/prve-spustenie/02.png) [![Práva na prístup ku kamere](assets/prve-spustenie/03.jpg)](assets/prve-spustenie/03.png)

### Uvítacia obrazovka

Ak si ešte nepoužíval aplikáciu HP Reveal (alebo kedysi Aurasma), zrejme nemáš účet. Na úvodnej obrazovke aplikácie preto klikni na modré tlačidlo **Create an account** a nasleduj ďalšie pokyny.

Pokiaľ už máš vytvorený účet, pokračuj nižšie na [pokyny k prihláseniu](#prihlasenie-existujuceho-hraca).

[![Úvodná obrazovka aplikácie](assets/prve-spustenie/04.jpg)](assets/prve-spustenie/04.png) 

### Registrácia nového hráča

Ako nový používateľ sa môžeš[^1] zadarmo zaregistrovať a používať pripravené aury. Potrebuješ k tomu vymyslieť **jedinečné meno používateľa** (nick) a **dobré heslo**[^2]. Aplikácia si na začiatku vypýta aj e-mailovú adresu, ale tá nie je povinnou položkou.

[![Zadanie mailovej adresy](assets/prve-spustenie/05.jpg)](assets/prve-spustenie/05.png) [![Voľba mena hráča](assets/prve-spustenie/06.jpg)](assets/prve-spustenie/06.png) [![Tvorba tajného hesla](assets/prve-spustenie/07.jpg)](assets/prve-spustenie/07.png)      

### Prihlásenie existujúceho hráča

Pokiaľ už máš svoje prihlasovacie údaje (napríklad z hrania inej Aury), môžeš na úvodnej stránke zvoliť modrý odkaz dole **Log in**. Ten ti zobrazí dve obrazovky, pričom najprv si vypýta používateľské meno a následne heslo.

[![Zadanie hráčskeho mena](assets/prve-spustenie/10.jpg)](assets/prve-spustenie/10.png)  [![Zadanie hesla](assets/prve-spustenie/11.jpg)](assets/prve-spustenie/11.png)

Ak si nepamätáš svoje meno, môžeš využiť odkaz pod vstupným políčkom _Forgot username? Click here_. Aplikácia si vypýta e-mailovú adresu (ak si ju pri registrácii zadal) a zobrazí ďalšie pokyny na obnovu prístupu k tvojmu účtu.

### Posledný skok a hotovo!

Pri uvítaní sa zobrazí tmavá vrstva, ktorá ťa informuje o možnosti vytvoriť auru. Túto možnosť **chceme teraz preskočiť**, preto klikneme vľavo hore na tlačidlo **Skip**. Ak vidíš na obrazovke voľne dostupné aury, máš za sebou prvé spustenie aplikácie. Pre hranie hry nejakej kešky je však potrebné spárovať svoj účet s pripravenými materiálmi. Viac sa o tom dozvieš [v ďalšom kroku](priprava-aury).

[![Auru vytvárať nebudeme](assets/prve-spustenie/08.jpg)](assets/prve-spustenie/08.png)  [![Prehľad dostupného obsahu v aplikácii](assets/prve-spustenie/09.jpg)](assets/prve-spustenie/09.png) 

[^1]: Registrácia do appky HP Reveal pre hráčov **nie je povinná**, no v takom prípade sa môže správať neštandardne. Odporúčame preto registráciu, aby si predišiel možným problémom pri hraní. Ak chceš napriek tomu preskočiť registráciu, klikni na [Uvítacej obrazovke](#uvitacia-obrazovka) vpravo hore na tlačidlo **Skip**.   
[^2]: Chceš vedieť, ako by malo vyzerať správne heslo? Prečítaj si [tieto tipy](https://support.google.com/accounts/answer/32040?hl=sk)!

## iOS

Prvé spustenie aplikácie na systéme iOS by malo prebiehať veľmi podobne. Spôsob získania prístupových práv k súborom a lokalizácii zariadenia sa môže líšiť. Od časti [Uvítacia obrazovka](#uvitacia-obrazovka) by malo ísť o identický postup.

!!! note "Detailný návod nedostupný"
    Ľutujeme, ale momentálne nemáme dostupné ukážky používania na iOS. Ak nám chceš pomôcť ich doplniť, prečítaj si o tom, [ako pridať nový obsah](../chcem-pomoct). Ďakujeme!