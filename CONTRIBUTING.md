# Pre prispievateľov

Stránka je generovaná prostredníctvom utility MkDocs. Celý obsah stránky sa nachádza v adresári `docs/` a je tvorený súbormi vo formáte Markdown (*.md). 

Odporúčané zdroje (anglicky):
* [príručka ako písať v Markdowne](https://guides.github.com/features/mastering-markdown/) 
* [dokumentácia používania MkDocs](https://www.mkdocs.org/user-guide/writing-your-docs/).

Po úprave je potrebné vytvoriť merge-request, ktorý niektorý z adminov projektu schváli. Ihneď po schválení sa generuje nová verzia, ktorá je do niekoľkých minút dostupná online pre všetkých.

* [git tutorial](https://www.atlassian.com/git/tutorials/what-is-version-control) (konkurenčná služba, ale má to pekne spracované)